package com.JummumCo.Jummum.Activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.JummumCo.Jummum.Interface.IHttpCallback;
import com.JummumCo.Jummum.Manager.PreferenceManager;
import com.JummumCo.Jummum.Model.UserAccountAuthenResultData;
import com.JummumCo.Jummum.Respository.MemberRepository;
import com.JummumCo.Jummum.Util.Util;
import com.android.jummum.R;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends BaseActivity {

    @BindView(R.id.appBar)
    AppBarLayout appBar;
    @BindView(R.id.txt_username)
    EditText txtUsername;
    @BindView(R.id.txt_password)
    EditText txtPassword;
    @BindView(R.id.btnLogin)
    Button btnLogin;
    @BindView(R.id.layout_register)
    LinearLayout layoutRegister;
    @BindView(R.id.layout_forget_password)
    RelativeLayout layoutForgetPassword;
    @BindView(R.id.main_content)
    LinearLayout mainContent;
    @BindView(R.id.login_button_fb)
    LoginButton loginButtonFb;
    private MemberRepository memberRepository;
    private CallbackManager callbackManager;
    private AccessToken mAccessToken;
    private String token;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        init();
    }

    private void init() {

        memberRepository = new MemberRepository();

        callbackManager = CallbackManager.Factory.create();
        loginButtonFb.setReadPermissions("email", "user_birthday");
        token = FirebaseInstanceId.getInstance().getToken();
        loginButtonFb.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                mAccessToken = loginResult.getAccessToken();
                getUserProfile(mAccessToken);
            }

            @Override
            public void onCancel() {
                Log.i("fb = ", "cancle");
            }

            @Override
            public void onError(FacebookException error) {
                Log.i("fb = ", error.toString());
            }
        });
        //login();


    }

    private void getUserProfile(AccessToken mAccessToken) {

        final Profile profile = Profile.getCurrentProfile();
        GraphRequest request = GraphRequest.newMeRequest(
                mAccessToken,
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        Log.v("LoginActivity", response.toString());
                        // Application code
//                        String email = object.getString("email");
//                        String birthday = object.getString("birthday"); // 01/31/1980 format

                        try {
                            memberRepository.getLoginFb(object.getString("email").toString(),
                                    "1",
                                    "",
                                    "",
                                    token,
                                    "",
                                    object.getString("name"),
                                    "",
                                    "0",
                                    object.getString("email").toString(),
                                    "",
                                    Util.getModifireDate(),
                                    object.getString("email").toString(),
                                    token,
                                    "create new account",
                                    "",
                                    new IHttpCallback<List<List<UserAccountAuthenResultData>>>() {
                                        @Override
                                        public void onSuccess(List<List<UserAccountAuthenResultData>> response) {

                                            if (PreferenceManager.getInstance().getSaveCreditCard() != null) {
                                                if (!response.get(0).get(0).getUserAccountID().equals(PreferenceManager.getInstance().getSaveCreditCard())) {
                                                    PreferenceManager.getInstance().setSaveCreditCard(null);
                                                }
                                            }

                                            PreferenceManager.getInstance().setMemberId(response.get(0).get(0).getUserAccountID());
                                            PreferenceManager.getInstance().setUsername(response.get(0).get(0).getUsername());
                                            PreferenceManager.getInstance().setToken(response.get(0).get(0).getDeviceToken());

                                            Log.i("xxx", "");
                                            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            startActivity(intent);
                                        }

                                        @Override
                                        public void onError(String message) {

                                        }
                                    });
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email,gender,birthday");
        request.setParameters(parameters);
        request.executeAsync();


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode,resultCode,data);
    }

    private void login() {
        //        String pass = "37bc854596cb721fca1da704fd385296bf881462013f09ffd80af72f0e92b5bc";
//        String username = "thidaporn.kijkamjai@gmail.com";
        String pass = txtPassword.getText().toString().trim();
        String username = txtUsername.getText().toString().trim();

        memberRepository.login(pass, username, username, token,"", new IHttpCallback<List<List<UserAccountAuthenResultData>>>() {
            @Override
            public void onSuccess(List<List<UserAccountAuthenResultData>> response) {

                if (response.get(0).size() > 0) {
                    UserAccountAuthenResultData resp = response.get(0).get(0);
                    if (resp.getUsername() != "") {

                        if (PreferenceManager.getInstance().getSaveCreditCard() != null) {
                            if (!resp.getUserAccountID().equals(PreferenceManager.getInstance().getSaveCreditCard())) {
                                PreferenceManager.getInstance().setSaveCreditCard(null);
                            }
                        }
                        PreferenceManager.getInstance().setMemberId(resp.getUserAccountID());
                        PreferenceManager.getInstance().setUsername(resp.getUsername());
                        PreferenceManager.getInstance().setToken(resp.getDeviceToken());

                        Log.i("xxx", "");
                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    } else {
                        Util.showAlert(LoginActivity.this, "Username หรือ Password ไม่ถูกต้อง", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                    }
                }else {
                    Util.showAlert(LoginActivity.this, "เกิดข้อผิดพลาด กรุณาทำรายการใหม่", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                }
            }

            @Override
            public void onError(String message) {
                Util.showAlert(LoginActivity.this, message, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
            }
        });




    }

    @OnClick(R.id.layout_register)
    public void onViewClickedRegister() {
        Intent intent = new Intent(LoginActivity.this, CreateUserActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.layout_forget_password)
    public void onViewClickedForgetPassword() {
        Intent intent = new Intent(LoginActivity.this, ForgetPasswordActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.btnLogin)
    public void onViewClickedLogin() {
        login();
    }
}
