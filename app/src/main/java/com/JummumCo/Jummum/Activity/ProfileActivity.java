package com.JummumCo.Jummum.Activity;

import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v7.app.AppCompatActivity;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.JummumCo.Jummum.Interface.IHttpCallback;
import com.JummumCo.Jummum.Respository.MemberRepository;
import com.JummumCo.Jummum.Manager.PreferenceManager;
import com.JummumCo.Jummum.Model.UserAccountAuthenResultData;
import com.android.jummum.R;
import com.JummumCo.Jummum.Util.Util;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ProfileActivity extends AppCompatActivity {

    @BindView(R.id.btn_back)
    RelativeLayout btnBack;
    @BindView(R.id.title_header)
    TextView titleHeader;
    @BindView(R.id.appBar)
    AppBarLayout appBar;
    @BindView(R.id.tv_email)
    TextView tvEmail;
    @BindView(R.id.tv_fullname)
    TextView tvFullname;
    @BindView(R.id.tv_birth_date)
    TextView tvBirthDate;
    @BindView(R.id.tv_tel)
    TextView tvTel;
    @BindView(R.id.main_content)
    LinearLayout mainContent;

    MemberRepository memberRepository;
    UserAccountAuthenResultData userAccountAuthenResultData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);

        init();
    }

    private void init() {
        memberRepository = new MemberRepository();

        memberRepository.getUserData(PreferenceManager.getInstance().getUserName(), PreferenceManager.getInstance().getToken(),
                PreferenceManager.getInstance().getUserName(), new IHttpCallback<List<List<UserAccountAuthenResultData>>>() {
                    @Override
                    public void onSuccess(List<List<UserAccountAuthenResultData>> response) {
                        userAccountAuthenResultData = response.get(0).get(0);
                        setView();
                    }

                    @Override
                    public void onError(String message) {
                        Util.showToast(mainContent,message);
                    }
                });
    }

    private void setView() {
        tvEmail.setText(userAccountAuthenResultData.getEmail().toString());
        tvFullname.setText(userAccountAuthenResultData.getFirstName() + "  "+userAccountAuthenResultData.getLirstName());
        tvBirthDate.setText(userAccountAuthenResultData.getBirthDate().substring(0,10));
        tvTel.setText(Util.phoneFormat(userAccountAuthenResultData.getPhoneNo()));
    }

    @OnClick(R.id.btn_back)
    public void onViewClickedBack() {
        finish();
    }
}
