package com.JummumCo.Jummum.Views.RecycleViewHolder;

/**
 * Created by likit on 13/7/2560.
 */

public interface IClickListener<T> {
    void onClick(T item);
}