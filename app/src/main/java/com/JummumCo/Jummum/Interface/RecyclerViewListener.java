package com.JummumCo.Jummum.Interface;

public interface RecyclerViewListener<T> {
    void onClick(T item);
    void onLoadMore();
}
