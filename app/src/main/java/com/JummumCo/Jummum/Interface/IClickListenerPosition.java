package com.JummumCo.Jummum.Interface;

/**
 * Created by likit on 13/7/2560.
 */

public interface IClickListenerPosition<T,X> {
    void onClick(T item,X position);
}